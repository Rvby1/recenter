package com.example.selfcareapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.ArrayList;

public class SleepCalcRecyclerAdapter extends RecyclerView.Adapter<SleepCalcRecyclerAdapter.SleepCalcViewHolder> {
    private ArrayList<SleepTime> sleepTimesList;
    private LayoutInflater inflater;
    private SleepCalcActivity sleepCalcActivity;

    public SleepCalcRecyclerAdapter(SleepCalcActivity sleepCalcActivity) {
        this.sleepTimesList = new ArrayList<>();
        this.inflater = LayoutInflater.from(sleepCalcActivity.getApplicationContext());
        this.sleepCalcActivity = sleepCalcActivity;
    }

    @NonNull
    @Override
    public SleepCalcViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View sleepTimeItemView = inflater.inflate(R.layout.sleep_time_recycler_item, viewGroup, false);
        return new SleepCalcViewHolder(sleepTimeItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SleepCalcViewHolder sleepCalcViewHolder, int i) {
        SleepTime currentSleepTime = this.sleepTimesList.get(i);
        sleepCalcViewHolder.sleepTimeTextView.setText(currentSleepTime.toString());
    }

    @Override
    public int getItemCount() {
        return sleepTimesList.size();
    }

    public void setSleepTimesList(ArrayList<SleepTime> sleepTimesList) {
        this.sleepTimesList = sleepTimesList;
        this.notifyDataSetChanged();
    }

    public class SleepCalcViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {
        private final TextView sleepTimeTextView;

        public SleepCalcViewHolder(@NonNull View itemView) {
            super(itemView);
            sleepTimeTextView = itemView.findViewById(R.id.sleepTimeTextView_sleepTimeRecyclerItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int indexOfClickedItem = getLayoutPosition();
            SleepTime clickedSleepTime = sleepTimesList.get(indexOfClickedItem);
            sleepCalcActivity.startAddAlarmActivity(clickedSleepTime);
        }
    }
}
