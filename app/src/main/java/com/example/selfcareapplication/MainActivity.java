package com.example.selfcareapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openMoodCard(View view) {
        Intent intent = new Intent(this, MoodCardActivity.class);
        startActivity(intent);
    }

    public void openSleepCalculator(View view) {
        Intent intent = new Intent(this, SleepCalcActivity.class);
        startActivity(intent);
    }

    public void startMeditationActivity(View view) {
        Intent intent = new Intent(this, MeditationSelectorActivity.class);
        startActivity(intent);
    }


    public void startSelfCareActivities(View view) {
        Intent intent = new Intent(this, SelfCareTaskSelectorActivity.class);
        startActivity(intent);
    }
}
