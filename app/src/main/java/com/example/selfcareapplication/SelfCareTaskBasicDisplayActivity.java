package com.example.selfcareapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SelfCareTaskBasicDisplayActivity extends AppCompatActivity {
    private String selfCareTaskTitle;
    private String selfCareTaskDescription;
    private SelfCareTaskType selfCareTaskType;

    private TextView selfCareTaskTitleTextView;
    private TextView selfCareTaskDescriptionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_care_task_basic_display);

        this.selfCareTaskTitleTextView = findViewById(R.id.taskTitleTextView_selfCareTaskBasicDisplay);
        this.selfCareTaskDescriptionTextView = findViewById(R.id.taskDescriptionTextView_selfCareTaskBasicDisplay);

        //get intent and retrieve data from it:
        Intent receivedIntent = getIntent();
        if(receivedIntent != null) {
            this.selfCareTaskTitle = receivedIntent.getStringExtra("Title");
            this.selfCareTaskDescription = receivedIntent.getStringExtra("Description");
            this.selfCareTaskType = (SelfCareTaskType) receivedIntent.getSerializableExtra("TaskType");
        } else {
            this.selfCareTaskTitle = "ERROR: NO INTENT DATA RECEIVED";
            this.selfCareTaskDescription = "ERROR: NO INTENT DATA RECEIVED";
            this.selfCareTaskType = SelfCareTaskType.GET_ACTIVE;
        }
        this.selfCareTaskTitleTextView.setText(this.selfCareTaskTitle);
        this.selfCareTaskDescriptionTextView.setText(this.selfCareTaskDescription);

        //set the image view to the proper image for the task type:
        ImageView selfCareTaskImageView = findViewById(R.id.iconImageView_selfCareTaskBasicDisplay);

        switch(selfCareTaskType) {
            case GET_ACTIVE:
                selfCareTaskImageView.setImageResource(R.drawable.weight);
                break;
            case WRITE_IT_OUT:
                selfCareTaskImageView.setImageResource(R.drawable.write);
                break;
            case REFLECT:
                selfCareTaskImageView.setImageResource(R.drawable.think);
                break;
        }
    }

    public void finishSelfCareTask(View view) {
        finish();
    }
}
