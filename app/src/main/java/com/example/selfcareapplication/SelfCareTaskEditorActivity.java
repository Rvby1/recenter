package com.example.selfcareapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class SelfCareTaskEditorActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private TextView headerTextView;
    private EditText selfCareTaskTitleEditText;
    private EditText selfCareTaskDescriptionEditText;
    private Spinner selfCareTaskTypeSpinner;
    private Button finishActivityBtn;

    private int selfCareTaskUID;
    private int requestCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_care_task_editor);

        //set up spinner:
        this.selfCareTaskTypeSpinner = findViewById(R.id.selfCareTaskTypeSpinner_selfCareTaskEditor);
        ArrayAdapter<CharSequence> selfCareTaskTypeAdapter = ArrayAdapter.createFromResource(this,
                R.array.selfCareTaskTypes_selfCareTaskSelector, R.layout.spinner_item);
        selfCareTaskTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.selfCareTaskTypeSpinner.setAdapter(selfCareTaskTypeAdapter);

        //get references to other needed views:
        this.headerTextView = findViewById(R.id.headerTextView_selfCareTaskEditor);
        this.selfCareTaskTitleEditText = findViewById(R.id.selfCareTaskTitleEditText_selfCareTaskEditor);
        this.selfCareTaskDescriptionEditText = findViewById(R.id.selfCareTaskDescriptionEditText_selfCareTaskEditor);
        this.finishActivityBtn = findViewById(R.id.finishEditingTaskBtn_selfCareTaskEditor);

        //get intent data:
        Intent receivedIntent = getIntent();
        if(receivedIntent != null) {
            requestCode = receivedIntent.getIntExtra(getString(R.string.requestCode_intentKey_selfCareTaskEditor), 0);

            //set data for editing existing tasks:
            if(requestCode == SelfCareTaskSelectorActivity.EDIT_SELFCARETASK_REQUESTCODE) {
                //set text to proper strings:
                this.headerTextView.setText(R.string.headerEditingTask_selfCareTaskEditor);
                this.finishActivityBtn.setText(R.string.finishBtnTextEditingTask_selfCareTaskEditor);

                //get data from self care task:
                this.selfCareTaskTitleEditText.setText(receivedIntent.getStringExtra(getString(R.string.taskTitle_intentKey_selfCareTaskEditor)));
                this.selfCareTaskDescriptionEditText.setText(receivedIntent.getStringExtra(getString(R.string.taskDescription_intentKey_selfCareTaskEditor)));
                SelfCareTaskType selfCareTaskType = (SelfCareTaskType) receivedIntent.getSerializableExtra(getString(R.string.taskType_intentKey_selfCareTaskEditor));
                this.selfCareTaskTypeSpinner.setSelection(selfCareTaskType.getValue());
                this.selfCareTaskUID = receivedIntent.getIntExtra(getString(R.string.taskUID_intentKey_selfCareTaskEditor), 0);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /*
        Called once the activity is finished.
        Post-cond:
            if the fields are not empty, creates intent with user-input data
            if any fields are empty, says result was cancelled and displays short toast
     */
    public void finishActivity(View view) {
        String selfCareTaskTitle = selfCareTaskTitleEditText.getText().toString();
        String selfCareTaskDescription = selfCareTaskDescriptionEditText.getText().toString();
        String selfCareTaskType = selfCareTaskTypeSpinner.getSelectedItem().toString();
        Intent intentToReturn = new Intent();

        //ensure that we're not creating an activity without necessary data:
        if(selfCareTaskTitle.equals("") || selfCareTaskDescription.equals("") || selfCareTaskType.equals("")) {
            setResult(RESULT_CANCELED, intentToReturn);
        }

        //put data from fields into intent:
        else {
            intentToReturn.putExtra(getString(R.string.taskTitle_intentKey_selfCareTaskEditor), selfCareTaskTitle);
            intentToReturn.putExtra(getString(R.string.taskDescription_intentKey_selfCareTaskEditor), selfCareTaskDescription);
            intentToReturn.putExtra(getString(R.string.taskType_intentKey_selfCareTaskEditor), selfCareTaskType);
            intentToReturn.putExtra(getString(R.string.taskUID_intentKey_selfCareTaskEditor), selfCareTaskUID);
            intentToReturn.putExtra(getString(R.string.requestCode_intentKey_selfCareTaskEditor), requestCode);
            setResult(RESULT_OK, intentToReturn);
        }
        finish();
    }
}
