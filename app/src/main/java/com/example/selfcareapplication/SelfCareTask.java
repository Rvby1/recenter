package com.example.selfcareapplication;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import java.util.Comparator;

@Entity(tableName = "self_care_task_table")
class SelfCareTask {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "unique_id")
    private int uniqueId;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "visible")
    private boolean visible;

    @ColumnInfo(name = "number_of_times_used")
    private int numberOfTimesUsed;

    @TypeConverters(SelfCareTaskTypeConverter.class)
    @ColumnInfo(name = "task_type")
    private SelfCareTaskType taskType;

    @ColumnInfo(name = "task_deleted")
    private boolean deleted;

    public SelfCareTask(String title, String description, boolean visible, SelfCareTaskType taskType) {
        this.title = title;
        this.description = description;
        this.visible = visible;
        this.taskType = taskType;
        //when a task ic created, it must have only been used 0 zero times before:
        this.numberOfTimesUsed = 0;
        //when a task is created, it must have not been deleted:
        this.deleted = false;
    }

    public SelfCareTask(String title, String description, boolean visible, SelfCareTaskType taskType, int UID) {
        this(title, description, visible, taskType);
        this.uniqueId = UID;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getNumberOfTimesUsed() {
        return numberOfTimesUsed;
    }

    public void setNumberOfTimesUsed(int numberOfTimesUsed) {
        this.numberOfTimesUsed = numberOfTimesUsed;
    }

    public SelfCareTaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(SelfCareTaskType taskType) {
        this.taskType = taskType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public static Comparator<SelfCareTask> titleLexicographicalComparator = new Comparator<SelfCareTask>() {
        @Override
        public int compare(SelfCareTask selfCareTaskToCompare1, SelfCareTask selfCareTaskToCompare2) {
            return selfCareTaskToCompare1.getTitle().compareTo(selfCareTaskToCompare2.getTitle());
        }
    };

    public static Comparator<SelfCareTask> numberOfTimesUsedComparator = new Comparator<SelfCareTask>() {
        @Override
        public int compare(SelfCareTask selfCareTaskToCompare1, SelfCareTask selfCareTaskToCompare2) {
            int numberOfTimesUsed1 = selfCareTaskToCompare1.getNumberOfTimesUsed();
            int numberOfTimesUsed2 = selfCareTaskToCompare2.getNumberOfTimesUsed();

            //if they are equal, sort on lexicographical order instead:
            if (numberOfTimesUsed1 == numberOfTimesUsed2) {
                return selfCareTaskToCompare1.getTitle().compareTo(selfCareTaskToCompare2.getTitle());
            }
            //otherwise, just sort them based on their number of times used:
            else if(numberOfTimesUsed1 > numberOfTimesUsed2) {
                return 1;
            } else {
                return 0;
            }
        }
    };

    public static Comparator<SelfCareTask> taskTypeComparator = new Comparator<SelfCareTask>() {
        @Override
        public int compare(SelfCareTask selfCareTaskToCompare1, SelfCareTask selfCareTaskToCompare2) {
            SelfCareTaskType selfCareTaskType1 = selfCareTaskToCompare1.getTaskType();
            SelfCareTaskType selfCareTaskType2 = selfCareTaskToCompare2.getTaskType();

            //if they are equal, sort on lexicographical order instead:
            if(selfCareTaskType1 == selfCareTaskType2) {
                return selfCareTaskToCompare1.getTitle().compareTo(selfCareTaskToCompare2.getTitle());
            }
            //otherwise, just compare the ordinal values of each of the enums:
            else if(selfCareTaskType1.ordinal() > selfCareTaskType2.ordinal()) {
                return 1;
            } else {
                return -1;
            }
        }
    };
}



/*
    Enum for the different kinds of self care tasks
 */
enum SelfCareTaskType {
    GET_ACTIVE (0),
    WRITE_IT_OUT (1),
    REFLECT (2)
    ;

    private int value;

    SelfCareTaskType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static SelfCareTaskType getSelfCareTaskTypeFromString(String taskTypeString) {
        switch(taskTypeString) {
            case "Get Active":
                return SelfCareTaskType.GET_ACTIVE;
            case "Write It Out":
                return SelfCareTaskType.WRITE_IT_OUT;
            case "Reflect":
                return SelfCareTaskType.REFLECT;
            default:
                return SelfCareTaskType.GET_ACTIVE;
        }
    }
}

class SelfCareTaskTypeConverter {
    @TypeConverter
    public static SelfCareTaskType fromSelfCareTaskTypeValue(int selfCareTaskTypeValue) {
        //TODO : MAKE THIS LOOK NICER?
        if(selfCareTaskTypeValue == 0) {
            return SelfCareTaskType.GET_ACTIVE;
        } else if(selfCareTaskTypeValue == 1) {
            return SelfCareTaskType.WRITE_IT_OUT;
        } else if(selfCareTaskTypeValue == 2) {
            return SelfCareTaskType.REFLECT;
        } else {
            return SelfCareTaskType.GET_ACTIVE;
        }
    }

    @TypeConverter
    public static int fromSelfCareTaskType(SelfCareTaskType selfCareTaskType) {
        return selfCareTaskType.getValue();
    }
}