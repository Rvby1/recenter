package com.example.selfcareapplication;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

import java.util.List;

public class SelfCareTaskSelectorActivity extends AppCompatActivity{
    private RecyclerView selfCareTaskSelectorRecycler;
    private SelfCareTaskRecyclerAdapter selfCareTaskSelectorRecyclerAdapter;
    private SelfCareTaskViewModel selfCareTaskViewModel;

    private Switch toggleShowHiddenTaskSwitch;
    private Switch toggleShowDeletedTasksSwitch;

    public static final int CREATE_SELFCARETASK_REQUESTCODE = 0;
    public static final int EDIT_SELFCARETASK_REQUESTCODE = 1;

    private final String showHiddenActivities_preferencesKey = "ShowHiddenTasks";
    private final String showDeletedActivities_preferencesKey =  "ShowDeletedTasks";

    private SharedPreferences activityPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_care_task_selector);

        this.selfCareTaskViewModel = ViewModelProviders.of(this).get(SelfCareTaskViewModel.class);

        //add listener for the selfCareTasks live data:
        this.selfCareTaskViewModel.getAllTasks().observe(this, new Observer<List<SelfCareTask>>() {
            @Override
            public void onChanged(@Nullable List<SelfCareTask> selfCareTasksList) {
                selfCareTaskSelectorRecyclerAdapter.setSelfCareTasksList(selfCareTasksList);
            }
        });

        //set up recycler view:
        this.selfCareTaskSelectorRecycler = findViewById(R.id.selfCareTasksRecycler_selfCareTaskSelector);
        this.selfCareTaskSelectorRecyclerAdapter = new SelfCareTaskRecyclerAdapter(this, this.selfCareTaskViewModel);
        this.selfCareTaskSelectorRecycler.setAdapter(selfCareTaskSelectorRecyclerAdapter);
        this.selfCareTaskSelectorRecycler.setLayoutManager(new LinearLayoutManager(this));
        registerForContextMenu(this.selfCareTaskSelectorRecycler);

        //get references to switches:
        this.toggleShowHiddenTaskSwitch = findViewById(R.id.toggleShowHiddenTasksSwitch_selfCareTaskSelector);
        this.toggleShowDeletedTasksSwitch = findViewById(R.id.toggleShowDeletedTasksSwitch_selfCareTaskSelector);

        //get activity preferences, then use them to set the show deleted/hidden tasks switches:
        //hidden tasks:
        this.activityPreferences = getPreferences(MODE_PRIVATE);
        boolean showHiddenTasks = this.activityPreferences.getBoolean(showHiddenActivities_preferencesKey, false);
        this.selfCareTaskSelectorRecyclerAdapter.setShowHiddenSelfCareTasks(showHiddenTasks);
        this.toggleShowHiddenTaskSwitch.setChecked(showHiddenTasks);
        //deleted tasks:
        boolean showDeletedTasks = this.activityPreferences.getBoolean(showDeletedActivities_preferencesKey, false);
        this.selfCareTaskSelectorRecyclerAdapter.setShowDeletedSelfCareTasks(showHiddenTasks);
        this.toggleShowDeletedTasksSwitch.setChecked(showDeletedTasks);
    }

    //context menu system:
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        SelfCareTask clickedSelfCareTask = selfCareTaskSelectorRecyclerAdapter.getLastClickedSelfCareTask();

        switch(item.getItemId()) {
            case R.id.edit_selfCareTask_contextMenu:
                startSelfCareTaskEditorActivity(clickedSelfCareTask);
                break;
            case R.id.hide_selfCareTask_contextMenu:
            case R.id.unhide_selfCareTask_contextMenu:
                selfCareTaskViewModel.toggleTaskVisibility(clickedSelfCareTask);
                break;
            case R.id.delete_selfCareTask_contextMenu:
            case R.id.restore_selfCareTask_contextMenu:
                selfCareTaskViewModel.toggleTaskDeleted(clickedSelfCareTask);
                break;

            case R.id.permanentlyDelete_selfCareTask_contextMenu:
                showConfirmDeleteTaskDialog(clickedSelfCareTask);
        }
        return super.onContextItemSelected(item);
    }

    /*
            Randomly selects a self care task and starts it.
         */
    public void startRandomSelfCareTask(View view) {
        selfCareTaskSelectorRecyclerAdapter.startRandomSelfCareTask();
    }

    /*
        Starts an activity that lets the user create a new self care activity
        Post-cond:
            If the user inputs proper data into the activity, then a new self care task is created and inserted into the database once
            they finish the activity.
            Otherwise, nothing happens.
     */
    public void startSelfCareTaskCreatorActivity(View view) {
        Intent createNewSelfCareTaskIntent = new Intent(this, SelfCareTaskEditorActivity.class);
        createNewSelfCareTaskIntent.putExtra(getString(R.string.requestCode_intentKey_selfCareTaskEditor), CREATE_SELFCARETASK_REQUESTCODE);
        this.startActivityForResult(createNewSelfCareTaskIntent, CREATE_SELFCARETASK_REQUESTCODE);
    }

    /*
        Starts an activity with the data of the selfCareTaskToEdit. This lets the user edit the data.
        Post-cond:
            Once the user finishes editing the task, the selfCareTaskToEdit is updated
     */
    public void startSelfCareTaskEditorActivity(SelfCareTask selfCareTaskToEdit) {
        Intent editSelfCareTaskIntent = new Intent(this, SelfCareTaskEditorActivity.class);
        editSelfCareTaskIntent.putExtra(getString(R.string.taskTitle_intentKey_selfCareTaskEditor), selfCareTaskToEdit.getTitle());
        editSelfCareTaskIntent.putExtra(getString(R.string.taskDescription_intentKey_selfCareTaskEditor), selfCareTaskToEdit.getDescription());
        editSelfCareTaskIntent.putExtra(getString(R.string.taskType_intentKey_selfCareTaskEditor), selfCareTaskToEdit.getTaskType());
        editSelfCareTaskIntent.putExtra(getString(R.string.taskUID_intentKey_selfCareTaskEditor), selfCareTaskToEdit.getUniqueId());
        editSelfCareTaskIntent.putExtra(getString(R.string.requestCode_intentKey_selfCareTaskEditor), EDIT_SELFCARETASK_REQUESTCODE);
        this.startActivityForResult(editSelfCareTaskIntent, EDIT_SELFCARETASK_REQUESTCODE);
    }

    /*
        Opens a self care task in a new display activity using the data from selfCareTaskToStart.
    */
    public void startSelfCareTaskDisplayActivity(SelfCareTask selfCareTaskToStart) {
        //increment the number of times that self care task has been opened:
        selfCareTaskViewModel.incrementTaskNumberOfTimesUsed(selfCareTaskToStart);

        //open self care activity with the proper data:
        Intent selfCareTaskDisplayIntent = new Intent(this, SelfCareTaskBasicDisplayActivity.class);
        selfCareTaskDisplayIntent.putExtra(getString(R.string.taskTitle_intentKey_selfCareTaskEditor), selfCareTaskToStart.getTitle());
        selfCareTaskDisplayIntent.putExtra(getString(R.string.taskDescription_intentKey_selfCareTaskEditor), selfCareTaskToStart.getDescription());
        selfCareTaskDisplayIntent.putExtra(getString(R.string.taskType_intentKey_selfCareTaskEditor), selfCareTaskToStart.getTaskType());
        this.startActivity(selfCareTaskDisplayIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent returnedIntent) {
        String returnedTaskTitle = returnedIntent.getStringExtra(getString(R.string.taskTitle_intentKey_selfCareTaskEditor));
        String returnedTaskDescription = returnedIntent.getStringExtra(getString(R.string.taskDescription_intentKey_selfCareTaskEditor));
        SelfCareTaskType returnedTaskType = SelfCareTaskType.getSelfCareTaskTypeFromString(returnedIntent.getStringExtra(getString(R.string.taskType_intentKey_selfCareTaskEditor)));
        int returnedTaskUID = returnedIntent.getIntExtra(getString(R.string.taskUID_intentKey_selfCareTaskEditor), 0);

        //result when user creates new self care task:
        if(requestCode == CREATE_SELFCARETASK_REQUESTCODE && resultCode == RESULT_OK) {
            SelfCareTask newSelfCareTask = new SelfCareTask(returnedTaskTitle, returnedTaskDescription, true, returnedTaskType);
            selfCareTaskViewModel.insertTask(newSelfCareTask);
        }

        //result when user edits self care task:
        else if (requestCode == EDIT_SELFCARETASK_REQUESTCODE && resultCode == RESULT_OK) {
            SelfCareTask updatedSelfCareTask = new SelfCareTask(returnedTaskTitle, returnedTaskDescription, true, returnedTaskType, returnedTaskUID);
            selfCareTaskViewModel.updateTask(updatedSelfCareTask);
        }

        //result when user does not enter all the needed data:
        else {
            Toast.makeText(this, "Data Missing, So No Task Added", Toast.LENGTH_SHORT);
        }
    }

    public void toggleShowHiddenTasks(View view) {
        boolean showHiddenTasks = toggleShowHiddenTaskSwitch.isChecked();
        selfCareTaskSelectorRecyclerAdapter.setShowHiddenSelfCareTasks(showHiddenTasks);
        SharedPreferences.Editor activityPreferencesEditor = activityPreferences.edit();
        activityPreferencesEditor.putBoolean(showHiddenActivities_preferencesKey, showHiddenTasks);
        activityPreferencesEditor.apply();
    }

    public void toggleShowDeletedTasks(View view) {
        boolean showDeletedTasks = toggleShowDeletedTasksSwitch.isChecked();
        selfCareTaskSelectorRecyclerAdapter.setShowDeletedSelfCareTasks(showDeletedTasks);
        SharedPreferences.Editor activityPreferencesEditor = activityPreferences.edit();
        activityPreferencesEditor.putBoolean(showDeletedActivities_preferencesKey, showDeletedTasks);
        activityPreferencesEditor.apply();
    }

    public void showConfirmDeleteTaskDialog(final SelfCareTask taskToDelete) {
        AlertDialog.Builder userConfirmationDialogBuilder = new AlertDialog.Builder(this);
        userConfirmationDialogBuilder.setTitle("Confirm Deletion");
        userConfirmationDialogBuilder.setMessage("Are you sure you want to delete this task?");

        //code for "Delete" button. Call method to delete task:
        userConfirmationDialogBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selfCareTaskViewModel.deleteTask(taskToDelete);
            }
        });

        //code for "Cancel" button:
        userConfirmationDialogBuilder.setNegativeButton("Cancel", null);
        AlertDialog userConfirmationDialog = userConfirmationDialogBuilder.create();
        userConfirmationDialog.show();
    }

}
