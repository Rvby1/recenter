package com.example.selfcareapplication;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.content.FileProvider;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MeditationSelectorActivity extends AppCompatActivity {
    private static final String LOG_TAG = MeditationSelectorActivity.class.getSimpleName();

    private final String fileProviderAuthority = "com.example.selfcareapplication.fileprovider";
    private final int WRITE_EXTERNAL_STORAGE_PERMISSIONS_REQUESTCODE = 1;

    private boolean externalStoragePermissionGranted;
    private File internalMeditationDirectory;
    private File externalMeditationDirectory;

    private MeditationFileAdapter meditationFileAdapter;
    private RecyclerView meditationFileRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meditation_selector);

        //KEEP?
        SharedPreferences meditationPreferences = getPreferences(MODE_PRIVATE);
        boolean firstInitializationDone = meditationPreferences.getBoolean("FirstInitializationDone", false);
        boolean newInternalAudioFilesAdded = meditationPreferences.getBoolean("NewInternalAudioFilesAdded", false);

        this.meditationFileRecyclerView = findViewById(R.id.meditationFileRecyclerView_meditationSelector);
        this.meditationFileRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.meditationFileAdapter = new MeditationFileAdapter(this);
        this.meditationFileRecyclerView.setAdapter(this.meditationFileAdapter);

        createMeditationFileDirectories();
        writeInternalMeditationsToInternalMemory();
        //refreshMeditationFileList();
        this.refreshMeditationFileList();
    }

    private void writeInternalMeditationsToInternalMemory() {
        new WriteFileToInternalFiles_Async(this, internalMeditationDirectory,"Breath_Sound_Body_Meditation.mp3", R.raw.breath_sound_body_meditation).execute();
        new WriteFileToInternalFiles_Async(this, internalMeditationDirectory,"breathing_meditation.mp3", R.raw.breathing_meditation).execute();
        new WriteFileToInternalFiles_Async(this, internalMeditationDirectory,"basic_meditation_spanish.mp3", R.raw.basic_meditation_spanish).execute();
        new WriteFileToInternalFiles_Async(this, internalMeditationDirectory,"complete_meditation_instructions.mp3", R.raw.complete_meditation_instructions).execute();
        new WriteFileToInternalFiles_Async(this, internalMeditationDirectory,"loving_kindness_meditation.mp3", R.raw.loving_kindness_meditation).execute();
    }

    /*
        Ensures that the external and internal dirs for meditation files are created
     */
    private void createMeditationFileDirectories() {
        //ensure that the internal meditations dir exists in internal files:
        internalMeditationDirectory = new File(this.getFilesDir(), "MeditationFiles");
        if(!internalMeditationDirectory.exists()) {
            if(!internalMeditationDirectory.mkdir()) {
                Log.e(LOG_TAG, "Failed to make internal meditations dir");
            }
        }

        //ensure that the external meditations dir exists in public MUSIC directory
        if(checkIfExternalStorageAvailable()){
            externalMeditationDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC), "MeditationFiles");
            askForExternalWritePermission();
            if(externalStoragePermissionGranted) {
                if (!externalMeditationDirectory.exists()) {
                    if (!externalMeditationDirectory.mkdirs()) {
                        Log.e(LOG_TAG, "Failed to make external meditations dir");
                    }
                }
            }
        }
    }

    public void refreshMeditationsListOnClick(View view) {
        refreshMeditationFileList();
    }

    public void refreshMeditationFileList() {
        new refreshMeditationFileListAsync(this, internalMeditationDirectory, externalMeditationDirectory).execute();
    }

    private class refreshMeditationFileListAsync extends AsyncTask<Void, Void, Void> {
        Context applicationContext;
        final File internalMeditationDirectory;
        final File externalMeditationDirectory;

        //TODO: IS THERE A BETTER/MORE EFFICIENT WAY TO HANDLE THE REFRESHING?

        public refreshMeditationFileListAsync(Context context, final File internalMeditationDirectory, final File externalMeditationDirectory) {
            this.applicationContext = context.getApplicationContext();
            this.internalMeditationDirectory = internalMeditationDirectory;
            this.externalMeditationDirectory = externalMeditationDirectory;
        }
        
        @Override
        protected Void doInBackground(Void... voids) {
            File[] internalMeditationFileList = null;
            File[] externalMeditationFileList = null;
            final File[] internalMeditationFileListFinal;
            final File[] externalMeditationFileListFinal;
            AudioFileFilter audioFileFilter = new AudioFileFilter(applicationContext);

            //get all files in internal meditations folder:
            if(internalMeditationDirectory != null && internalMeditationDirectory.exists()) {
                internalMeditationFileList = internalMeditationDirectory.listFiles(audioFileFilter);
            }

            //get all files in external meditations folder:
            if(externalMeditationDirectory != null && externalMeditationDirectory.exists()) {
                if(MeditationSelectorActivity.checkIfExternalStorageAvailable()){
                    externalMeditationFileList = externalMeditationDirectory.listFiles(audioFileFilter);
                }
            }
            //create final references to these arrays so that we can use them inside the runnable class:
            internalMeditationFileListFinal = internalMeditationFileList;
            externalMeditationFileListFinal = externalMeditationFileList;

            //ensure that we're only updating UI elements in the UI thread:
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //clear the meditations list before we start refreshing the list so that we ensure we get rid of missing files in the list
                    meditationFileAdapter.clearMeditationsList();

                    //insert internal and external meditation files into adapter:
                    if(internalMeditationFileListFinal != null) {
                        for(File internalMeditationFile : internalMeditationFileListFinal) {
                            meditationFileAdapter.insertMeditationFileIntoAdapterList(internalMeditationFile);
                        }
                    }

                    if(externalMeditationFileListFinal != null) {
                        for(File externalMeditationFile : externalMeditationFileListFinal) {
                            meditationFileAdapter.insertMeditationFileIntoAdapterList(externalMeditationFile);
                        }
                    }
                }
            });
            return null;
        }
    }

    private void askForExternalWritePermission() {
        //permission not yet granted
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            //permission has been denied before, so show an explanation to make sure the user knows why we want the permission, then ask for permission:
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new ShowOKInfoBox_Async("Permission Needed",
                        "We need the external write permission to create a file in your Music directory for you to store your meditation audio files in.",
                        this).execute();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_PERMISSIONS_REQUESTCODE);
            }
            //just straight up ask for permission since the user hasn't denied it before:
            else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_PERMISSIONS_REQUESTCODE);
            }
        }
        //permission has been granted before:
        else {
            externalStoragePermissionGranted = true;
        }
    }

    public static boolean checkIfExternalStorageAvailable() {
        String externalStorageState = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(externalStorageState) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(externalStorageState)) {
            return true;
        }
        Log.d(LOG_TAG, "EXTERNAL MEMORY UNAVAILABLE!");
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == WRITE_EXTERNAL_STORAGE_PERMISSIONS_REQUESTCODE) {
            //permission granted:
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                externalStoragePermissionGranted = true;
            }
            //permission denied:
            else {
                externalStoragePermissionGranted = false;
            }
        }
    }

    /*
        Creates an intent to play meditationToPlay through some audio playing app
        Pre-cond:
            meditationToPlay links to a file in an accessible part of internal memory
        Post-cond:
            sends an intent with a content URI pointing to meditationToPlay to audio playing apps
     */
    public void playMeditation(File meditationToPlay) {
        Intent playMeditationFileIntent = new Intent(Intent.ACTION_VIEW);

        Uri meditationToPlayContentUri = FileProvider.getUriForFile(this.getApplicationContext(), "com.example.selfcareapplication.fileprovider", meditationToPlay);
        playMeditationFileIntent.setDataAndType(meditationToPlayContentUri, "audio/*");
        playMeditationFileIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if(playMeditationFileIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(playMeditationFileIntent);
        } else {
            Log.i(LOG_TAG, "Could not resolve activity. No audio players?");
        }
    }

    private class AudioFileFilter implements FileFilter {
        Context applicationContext;
        public AudioFileFilter (Context applicationContext) {
            this.applicationContext = applicationContext;
        }

        @Override
        public boolean accept(File fileToCheck) {
            Uri fileToCheckUri = FileProvider.getUriForFile(applicationContext, fileProviderAuthority, fileToCheck);
            String fileToCheckExtension = MimeTypeMap.getFileExtensionFromUrl(fileToCheckUri.toString());
            String fileToCheckFileType =  MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileToCheckExtension).split("/")[0];
            if(fileToCheckFileType.equals("audio"))
            {
                return true;
            }
            return false;
        }
    }

    private class WriteFileToInternalFiles_Async extends AsyncTask<Void, Void, Void> {
        private Context applicationContext;
        private File internalMeditationDirectory;
        private String fileName;
        private int fileRawId;

        public WriteFileToInternalFiles_Async(Context context, File internalMeditationDirectory, String fileName, int fileRawId) {
            this.applicationContext = context.getApplicationContext();
            this.fileName = fileName;
            this.fileRawId = fileRawId;
            this.internalMeditationDirectory = internalMeditationDirectory;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            File whereToWriteFile = new File(internalMeditationDirectory, fileName);

            //don't try to write file if it already exists:
            if(whereToWriteFile.exists()) {
                Log.i(LOG_TAG, fileName + " was previously written to " + whereToWriteFile.getPath());
            }
            //otherwise, try to write the file:
            else {
                try {
                    whereToWriteFile.createNewFile();
                    FileOutputStream outputStreamFromFile = new FileOutputStream(whereToWriteFile);
                    InputStream inputStreamFromRawAudioFile = applicationContext.getResources().openRawResource(fileRawId);

                    //write to buffer, then write buffer to internal memory:
                    int numBytesRead;
                    byte[] bufferForFileTransfer = new byte[51200];
                    while((numBytesRead = inputStreamFromRawAudioFile.read(bufferForFileTransfer)) != -1) {
                        outputStreamFromFile.write(bufferForFileTransfer, 0, numBytesRead);
                    }
                    inputStreamFromRawAudioFile.close();
                    outputStreamFromFile.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG," ERROR WRITING " + fileName + " TO " + whereToWriteFile.getPath());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            refreshMeditationFileList();
        }
    }

    private static class ShowOKInfoBox_Async extends AsyncTask<Void, Void, Void> {
        private String dialogBoxTitle;
        private String dialogBoxInfo;
        private Context applicationContext;

        public ShowOKInfoBox_Async(String dialogBoxTitle, String dialogBoxInfo, Context context) {
            this.dialogBoxTitle = dialogBoxTitle;
            this.dialogBoxInfo = dialogBoxInfo;
            this.applicationContext =  context.getApplicationContext();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AlertDialog.Builder dialogBox = new AlertDialog.Builder(applicationContext);
            dialogBox.setTitle(dialogBoxTitle);
            dialogBox.setMessage(dialogBoxInfo);
            dialogBox.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //do nothing here
                    }
                });
            dialogBox.setCancelable(true);
            dialogBox.create().show();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
