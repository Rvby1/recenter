package com.example.selfcareapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SelfCareTaskRecyclerAdapter extends RecyclerView.Adapter<SelfCareTaskRecyclerAdapter.SelfCareTaskViewHolder>{

    private static final String TAG = "SelfCareTaskRecyclerAdapter";

    private SelfCareTaskSelectorActivity selfCareTaskSelectorActivity;
    private LayoutInflater viewHolderInflater;
    private SelfCareTaskViewModel selfCareTaskViewModel;

    private List<SelfCareTask> selfCareTasksList;
    private boolean showHiddenSelfCareTasks;
    private boolean showDeletedSelfCareTasks;

    private int positionOfLastClickedItem; //used by context menu

    public SelfCareTaskRecyclerAdapter(SelfCareTaskSelectorActivity selfCareTaskSelectorActivity, SelfCareTaskViewModel selfCareTaskViewModel)
    {
        this.viewHolderInflater = LayoutInflater.from(selfCareTaskSelectorActivity.getApplicationContext());
        this.selfCareTaskSelectorActivity = selfCareTaskSelectorActivity;
        this.selfCareTaskViewModel = selfCareTaskViewModel;
        this.selfCareTasksList = new ArrayList<>();
    }

    public SelfCareTask getLastClickedSelfCareTask() {

        return selfCareTasksList.get(positionOfLastClickedItem);
    }


    @NonNull
    @Override
    public SelfCareTaskRecyclerAdapter.SelfCareTaskViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View recyclerItemView = viewHolderInflater.inflate(R.layout.self_care_task_recycler_item, viewGroup, false);
        return new SelfCareTaskViewHolder(recyclerItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SelfCareTaskRecyclerAdapter.SelfCareTaskViewHolder selfCareActivitiesViewHolder, int i) {
        SelfCareTask currentSelfCareTask = this.selfCareTasksList.get(i);
        //we should only try to show a task if it isn't deleted or if we're showing deleted tasks:
        if(!currentSelfCareTask.isDeleted() || showDeletedSelfCareTasks) {
            //we should also only try to show a task if it isn't visible or if we're showing invisible tasks:
            if(currentSelfCareTask.isVisible() || showHiddenSelfCareTasks) {
                //ensure that views aren't hidden when they shouldn't be:
                selfCareActivitiesViewHolder.selfCareTaskTitleTextView.setVisibility(View.VISIBLE);
                /*
                selfCareActivitiesViewHolder.selfCareTaskHideBtn.setVisibility(View.VISIBLE);
                selfCareActivitiesViewHolder.selfCareTaskDeleteBtn.setVisibility(View.VISIBLE);
                selfCareActivitiesViewHolder.selfCareTaskEditBtn.setVisibility(View.VISIBLE);*/
                //don't show permanently delete:

                String title = currentSelfCareTask.getTitle();
                selfCareActivitiesViewHolder.selfCareTaskTitleTextView.setText(title);
            }

            //don't show a task if it's invisible and we're not showing invisible tasks:
            else {
                selfCareActivitiesViewHolder.selfCareTaskTitleTextView.setVisibility(View.GONE);
                /*
                selfCareActivitiesViewHolder.selfCareTaskHideBtn.setVisibility(View.GONE);
                selfCareActivitiesViewHolder.selfCareTaskDeleteBtn.setVisibility(View.GONE);
                selfCareActivitiesViewHolder.selfCareTaskEditBtn.setVisibility(View.GONE);*/
            }
        }
        //don't show a task if it's deleted and we're not showing deleted tasks:
        else {
            selfCareActivitiesViewHolder.selfCareTaskTitleTextView.setVisibility(View.GONE);
            /*
            selfCareActivitiesViewHolder.selfCareTaskHideBtn.setVisibility(View.GONE);
            selfCareActivitiesViewHolder.selfCareTaskDeleteBtn.setVisibility(View.GONE);
            selfCareActivitiesViewHolder.selfCareTaskEditBtn.setVisibility(View.GONE);*/
        }

        /*
        //set hide button to have proper text (hide if task is unhidden, unhide if task is hidden):
        if(currentSelfCareTask.isVisible()) {
            selfCareActivitiesViewHolder.selfCareTaskHideBtn.setText("Hide");
        } else {
            selfCareActivitiesViewHolder.selfCareTaskHideBtn.setText("Unhide");
        }

        //set delete button to have proper text (delete if task is not deleted, undelete if task is deleted):
        if(currentSelfCareTask.isDeleted()) {
            selfCareActivitiesViewHolder.selfCareTaskDeleteBtn.setText("Restore");
        } else {
            selfCareActivitiesViewHolder.selfCareTaskDeleteBtn.setText("Delete");
        }
        */
    }

    @Override
    public int getItemCount() {
        return this.selfCareTasksList.size();
    }


    public void setShowHiddenSelfCareTasks(boolean showHiddenSelfCareTasks) {
        this.showHiddenSelfCareTasks = showHiddenSelfCareTasks;
        this.notifyDataSetChanged();
    }

    public void setShowDeletedSelfCareTasks(boolean showDeletedSelfCareTasks) {
        this.showDeletedSelfCareTasks = showDeletedSelfCareTasks;
        this.notifyDataSetChanged();
    }

    public void setSelfCareTasksList (List<SelfCareTask> selfCareTasksList) {
        this.selfCareTasksList = selfCareTasksList;
        this.notifyDataSetChanged();
    }

    /*
        Selects random self care task from the list of self care tasks and starts that task.
            Post-cond:
                if list is of size zero, then nothing happens.
                if list is of size > zero, a random activity is selected and started.
     */
    public void startRandomSelfCareTask() {
        int indexOfRandomSelfCareTask = (int)Math.floor(Math.random() * (selfCareTasksList.size()));
        selfCareTaskSelectorActivity.startSelfCareTaskDisplayActivity(selfCareTasksList.get(indexOfRandomSelfCareTask));
    }


    public class SelfCareTaskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener, View.OnLongClickListener {
        private final TextView selfCareTaskTitleTextView;
        private final Button selfCareTaskHideBtn;
        private final Button selfCareTaskDeleteBtn;
        private final Button selfCareTaskEditBtn;

        public SelfCareTaskViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnCreateContextMenuListener(this);
            //get views:
            this.selfCareTaskTitleTextView = itemView.findViewById(R.id.titleTextView_selfCareTaskItem);
            this.selfCareTaskHideBtn = itemView.findViewById(R.id.hideTaskBtn_selfCareTaskItem);
            this.selfCareTaskDeleteBtn = itemView.findViewById(R.id.deleteTaskBtn_selfCareTaskItem);
            this.selfCareTaskEditBtn = itemView.findViewById(R.id.editTaskBtn_selfCareTaskItem);

            //set click listeners:
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            this.selfCareTaskHideBtn.setOnClickListener(this);
            this.selfCareTaskEditBtn.setOnClickListener(this);
            this.selfCareTaskDeleteBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int viewId = view.getId();
            int indexOfClickedItem = getLayoutPosition();
            SelfCareTask clickedSelfCareTask = selfCareTasksList.get(indexOfClickedItem);

            if(viewId == R.id.hideTaskBtn_selfCareTaskItem) {
                selfCareTaskViewModel.toggleTaskVisibility(clickedSelfCareTask);
            } else if (viewId == R.id.deleteTaskBtn_selfCareTaskItem) {
                //if the task has already been deleted, we should fully delete the activity if the person wants us to:
                if(clickedSelfCareTask.isDeleted()) {
                    selfCareTaskSelectorActivity.showConfirmDeleteTaskDialog(clickedSelfCareTask);
                }

                //otherwise, we just want to set the task's deleted
                else {
                    selfCareTaskViewModel.toggleTaskDeleted(clickedSelfCareTask); //IMPLEMENT
                }
            } else if(viewId == R.id.editTaskBtn_selfCareTaskItem) {
                selfCareTaskSelectorActivity.startSelfCareTaskEditorActivity(clickedSelfCareTask);
            } else if(viewId == this.getItemId()) {
                selfCareTaskSelectorActivity.startSelfCareTaskDisplayActivity(clickedSelfCareTask);
            }
        }

        @Override
        public boolean onLongClick(View view) {
            positionOfLastClickedItem = getLayoutPosition();
            return false;
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            MenuInflater menuInflater = selfCareTaskSelectorActivity.getMenuInflater();
            menuInflater.inflate(R.menu.selfcaretask_context_menu, contextMenu);

            SelfCareTask clickedSelfCareTask = selfCareTasksList.get(positionOfLastClickedItem);
            boolean clickedSelfCareTaskVisible = clickedSelfCareTask.isVisible();
            boolean clickedSelfCareTaskDeleted = clickedSelfCareTask.isDeleted();

            //set the visibility of the hide or unhide context menu items based on the task visibility:
            contextMenu.findItem(R.id.hide_selfCareTask_contextMenu).setVisible(clickedSelfCareTaskVisible); //if task is visible, show item to hide it
            contextMenu.findItem(R.id.unhide_selfCareTask_contextMenu).setVisible(!clickedSelfCareTaskVisible);

            //set the visibility of the delete or recover context menu items based on the task deleted:
            contextMenu.findItem(R.id.delete_selfCareTask_contextMenu).setVisible(!clickedSelfCareTaskDeleted); //if task isn't deleted, show item to delete it
            contextMenu.findItem(R.id.restore_selfCareTask_contextMenu).setVisible(clickedSelfCareTaskDeleted);
            contextMenu.findItem(R.id.permanentlyDelete_selfCareTask_contextMenu).setVisible(clickedSelfCareTaskDeleted);
        }
    }
}
