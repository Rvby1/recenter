package com.example.selfcareapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class MeditationFileAdapter extends RecyclerView.Adapter<MeditationFileAdapter.MeditationFileViewHolder> {
    private static final String TAG = "MeditationFileAdapter";

    private MeditationSelectorActivity meditationSelectorActivity;
    private LayoutInflater viewInflater;

    private ArrayList<AudioFileWrapper> meditationFileList;

    public MeditationFileAdapter(MeditationSelectorActivity meditationSelectorActivity) {
        this.meditationSelectorActivity = meditationSelectorActivity;
        this.viewInflater = LayoutInflater.from(meditationSelectorActivity.getApplicationContext());
        this.meditationFileList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MeditationFileViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View recyclerItemView = viewInflater.inflate(R.layout.meditation_audiofile_recycler_item, viewGroup, false);
        return new MeditationFileViewHolder(recyclerItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MeditationFileViewHolder meditationFileViewHolder, int i) {
        AudioFileWrapper currentMeditationFile = meditationFileList.get(i);
        meditationFileViewHolder.meditationFileTitleTextView.setText(currentMeditationFile.getFileTitle());
        meditationFileViewHolder.meditationFileArtistNameTextView.setText(currentMeditationFile.getFileArtistName());
        meditationFileViewHolder.meditationFileTrackLengthTextView.setText(currentMeditationFile.getFileTrackLength());
        currentMeditationFile.setAlbumArtImageView(meditationFileViewHolder.meditationFileImageImageView);
    }

    @Override
    public int getItemCount() {
        return meditationFileList.size();
    }

    /*
        Checks if meditationFile is already in the meditationFileList. If it isn't, the meditationFile is inserted into the list.
        Pre-cond:
            meditationFile points to an audio/* file

        Post-cond:
            if meditationFile is not already in the meditationFileList, it is added to the list
     */
    public void insertMeditationFileIntoAdapterList(File meditationFile) {
        //check if meditationFile is already in list:
        for(AudioFileWrapper storedMeditationFile : meditationFileList) {
            if(storedMeditationFile.getFileLocation().getPath().equals(meditationFile.getPath())) {
                return;
            }
        }
        meditationFileList.add(new AudioFileWrapper(meditationFile));
        this.notifyItemInserted(meditationFileList.size() - 1);
    }

    public void clearMeditationsList() {
        meditationFileList.clear();
        this.notifyDataSetChanged();
    }

    public class MeditationFileViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView meditationFileTitleTextView;
        private final TextView meditationFileArtistNameTextView;
        private final TextView meditationFileTrackLengthTextView;
        private final ImageView meditationFileImageImageView;

        public MeditationFileViewHolder(@NonNull View itemView) {
            super(itemView);

            this.meditationFileTitleTextView = itemView.findViewById(R.id.titleTextView_meditationRecyclerItem);
            this.meditationFileArtistNameTextView = itemView.findViewById(R.id.artistNameTextView_meditationRecyclerItem);
            this.meditationFileTrackLengthTextView = itemView.findViewById(R.id.trackLengthTextView_meditationRecyclerItem);
            this.meditationFileImageImageView = itemView.findViewById(R.id.trackImageImageView_meditationRecyclerItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int indexOfClickedItem = getLayoutPosition();
            File clickedMeditationFile = meditationFileList.get(indexOfClickedItem).getFileLocation();
            //play music file if it exists:
            if(clickedMeditationFile.exists()) {
                meditationSelectorActivity.playMeditation(clickedMeditationFile);
            }
            //otherwise, notify the user and refresh the music list so that the music file gets removed:
            else {
                Toast.makeText(meditationSelectorActivity.getApplicationContext(),  "That file no longer exists! Refreshing!", Toast.LENGTH_SHORT).show();
                meditationSelectorActivity.refreshMeditationFileList();
            }
        }
    }
    //insert function that takes in File. Checks if file is already in list. If it isn't, then it adds it.
}
