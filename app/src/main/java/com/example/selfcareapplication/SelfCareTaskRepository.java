package com.example.selfcareapplication;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class SelfCareTaskRepository {
    private SelfCareTaskDAO selfCareTaskDAO;
    private LiveData<List<SelfCareTask>> allSelfCareTasks;

    public SelfCareTaskRepository(Application application) {
        SelfCareTaskDatabase selfCareTaskDatabase = SelfCareTaskDatabase.getDatabase(application);
        this.selfCareTaskDAO = selfCareTaskDatabase.selfCareTaskDAO();
        this.allSelfCareTasks = this.selfCareTaskDAO.getAllTasks();
    }

    public LiveData<List<SelfCareTask>> getAllTasks() {
        return allSelfCareTasks;
    }

    public void insertTask(SelfCareTask selfCareTaskToInsert) {
        new insertTaskAsync(selfCareTaskDAO).execute(selfCareTaskToInsert);
    }
    
    public void toggleTaskVisibility(SelfCareTask selfCareTaskToToggleVisibility) {
        new toggleTaskVisibilityAsync(selfCareTaskDAO).execute(selfCareTaskToToggleVisibility);
    }

    public void updateTask(SelfCareTask selfCareTaskToUpdate) {
        new updateTaskAsync(selfCareTaskDAO).execute(selfCareTaskToUpdate);
    }

    public void incrementTaskNumberOfTimesUsed(SelfCareTask selfCareTaskToIncrement) {
        new incrementTaskNumberOfTimesUsedAsync(selfCareTaskDAO).execute(selfCareTaskToIncrement);
    }

    public void toggleTaskDeleted(SelfCareTask selfCareTaskToToggleDelete) {
        new toggleTaskDeletedAsync(selfCareTaskDAO).execute(selfCareTaskToToggleDelete);
    }

    public void deleteTask(SelfCareTask selfCareTaskToDelete) {
        new deleteTaskAsync(selfCareTaskDAO).execute(selfCareTaskToDelete);
    }

    private static class updateTaskAsync extends AsyncTask<SelfCareTask, Void, Void> {
        private SelfCareTaskDAO selfCareTaskDAO;

        public updateTaskAsync(SelfCareTaskDAO selfCareTaskDAO) {
            this.selfCareTaskDAO = selfCareTaskDAO;
        }

        @Override
        protected Void doInBackground(SelfCareTask... selfCareTasks){
            selfCareTaskDAO.updateTask(selfCareTasks[0]);
            return null;
        }
    }

    private static class insertTaskAsync extends AsyncTask<SelfCareTask, Void, Void> {
        private SelfCareTaskDAO selfCareTaskDAO;

        public insertTaskAsync(SelfCareTaskDAO selfCareTaskDAO) {
            this.selfCareTaskDAO = selfCareTaskDAO;
        }

        @Override
        protected Void doInBackground(SelfCareTask... selfCareTasks){
            selfCareTaskDAO.insertTask(selfCareTasks[0]);
            return null;
        }
    }

    private static class toggleTaskVisibilityAsync extends AsyncTask<SelfCareTask, Void, Void> {
        private SelfCareTaskDAO selfCareTaskDAO;

        public toggleTaskVisibilityAsync(SelfCareTaskDAO selfCareTaskDAO) {
            this.selfCareTaskDAO = selfCareTaskDAO;
        }

        @Override
        protected Void doInBackground(SelfCareTask... selfCareTasks) {
            SelfCareTask selfCareTaskToToggleVisibility = selfCareTasks[0];
            //toggle visibility:
            selfCareTaskDAO.setTaskVisibility(selfCareTaskToToggleVisibility.getUniqueId(),
                        !selfCareTaskToToggleVisibility.isVisible());
                return null;
        }
    }

    private static class incrementTaskNumberOfTimesUsedAsync extends AsyncTask<SelfCareTask, Void, Void> {
        private SelfCareTaskDAO selfCareTaskDAO;

        public incrementTaskNumberOfTimesUsedAsync(SelfCareTaskDAO selfCareTaskDAO) {
            this.selfCareTaskDAO = selfCareTaskDAO;
        }

        @Override
        protected Void doInBackground(SelfCareTask... selfCareTasks) {
            SelfCareTask selfCareTaskToIncrement = selfCareTasks[0];
            //toggle visibility:
            selfCareTaskDAO.setTaskNumberOfTimesUsed(selfCareTaskToIncrement.getUniqueId(),
                    selfCareTaskToIncrement.getNumberOfTimesUsed() + 1);
            return null;
        }
    }

    private static class toggleTaskDeletedAsync extends AsyncTask<SelfCareTask, Void, Void> {
        private SelfCareTaskDAO selfCareTaskDAO;

        public toggleTaskDeletedAsync(SelfCareTaskDAO selfCareTaskDAO) {
            this.selfCareTaskDAO = selfCareTaskDAO;
        }

        @Override
        protected Void doInBackground(SelfCareTask... selfCareTasks) {
            SelfCareTask selfCareTaskToToggleDeleted = selfCareTasks[0];
            //toggle visibility:
            selfCareTaskDAO.setTaskDeleted(selfCareTaskToToggleDeleted.getUniqueId(),
                    !selfCareTaskToToggleDeleted.isDeleted());
            return null;
        }
    }

    private static class deleteTaskAsync extends AsyncTask<SelfCareTask, Void, Void> {
        private SelfCareTaskDAO selfCareTaskDAO;

        public deleteTaskAsync(SelfCareTaskDAO selfCareTaskDAO) {
            this.selfCareTaskDAO = selfCareTaskDAO;
        }

        @Override
        protected Void doInBackground(SelfCareTask... selfCareTasks) {
            SelfCareTask selfCareTaskToDelete = selfCareTasks[0];
            //toggle visibility:
            selfCareTaskDAO.delete(selfCareTaskToDelete);
            return null;
        }
    }


    //TODO: CREATE METHOD FOR DELETION
}
