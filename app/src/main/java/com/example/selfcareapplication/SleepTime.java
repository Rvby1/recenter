package com.example.selfcareapplication;

import android.net.Uri;

import java.util.Calendar;

public class SleepTime {
    private int minute;
    private int hour;
    private int AMPM;

    public SleepTime(int hour, int minute, int AMPM) {
        this.minute = minute;
        this.hour = hour;
        this.AMPM = AMPM;
    }

    public String toString() {
        String stringToReturn = "";

        if(hour == 0) {
            stringToReturn += "12:";
        } else {
            stringToReturn += hour + ":";
        }

        if(minute < 10 && minute >= 0) {
            stringToReturn += "0" + minute;
        } else {
            stringToReturn += "" + minute;
        }

        if(AMPM == Calendar.AM) {
            stringToReturn += " AM";
        } else {
            stringToReturn += " PM";
        }
        return stringToReturn;
    }


    public int getMinute() {
        return minute;
    }

    public int getHour24HourFormat() {
        if(AMPM == Calendar.AM) {
            return hour;
        } else {
            return hour + 12;
        }
    }
}
