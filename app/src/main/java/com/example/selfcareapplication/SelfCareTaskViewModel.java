package com.example.selfcareapplication;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.util.List;

public class SelfCareTaskViewModel extends AndroidViewModel {
    private SelfCareTaskRepository selfCareTaskRepository;
    private LiveData<List<SelfCareTask>> allSelfCareTasks;

    public SelfCareTaskViewModel(@NonNull Application application) {
        super(application);
        this.selfCareTaskRepository = new SelfCareTaskRepository(application);
        this.allSelfCareTasks = selfCareTaskRepository.getAllTasks();
    }

    public LiveData<List<SelfCareTask>> getAllTasks() {
        return allSelfCareTasks;
    }

    public void insertTask(SelfCareTask selfCareTaskToInsert) {
        selfCareTaskRepository.insertTask(selfCareTaskToInsert);
    }

    public void updateTask(SelfCareTask selfCareTaskToUpdate) {
        selfCareTaskRepository.updateTask(selfCareTaskToUpdate);
    }

    public void toggleTaskVisibility(SelfCareTask selfCareTaskToToggleVisibility) {
        selfCareTaskRepository.toggleTaskVisibility(selfCareTaskToToggleVisibility);
    }

    public void incrementTaskNumberOfTimesUsed(SelfCareTask selfCareTaskToIncrement) {
        selfCareTaskRepository.incrementTaskNumberOfTimesUsed(selfCareTaskToIncrement);
    }

    public void toggleTaskDeleted(SelfCareTask selfCareTaskToToggleDelete) {
        selfCareTaskRepository.toggleTaskDeleted(selfCareTaskToToggleDelete);
    }

    public void deleteTask(SelfCareTask selfCareTaskToDelete) {
        Toast.makeText(getApplication(), "Deleted Task", Toast.LENGTH_LONG);
        selfCareTaskRepository.deleteTask(selfCareTaskToDelete);
    }


}
