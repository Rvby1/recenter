package com.example.selfcareapplication;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface SelfCareTaskDAO {
    @Query("SELECT * FROM self_care_task_table")
    LiveData<List<SelfCareTask>> getAllTasks();

    @Delete
    void delete(SelfCareTask selfCareTaskToDelete);

    @Insert
    void insertTask(SelfCareTask selfCareTask);

    @Update
    void updateTask(SelfCareTask selfCareTask);

    @Query("UPDATE self_care_task_table SET visible = :visibility WHERE unique_id LIKE :uniqueID")
    void setTaskVisibility(int uniqueID, boolean visibility);

    @Query("UPDATE self_care_task_table SET task_deleted = :taskDeleted WHERE unique_id LIKE :uniqueID")
    void setTaskDeleted(int uniqueID, boolean taskDeleted);

    @Query("UPDATE self_care_task_table SET number_of_times_used = :numberOfTimesUsed WHERE unique_id LIKE :uniqueID")
    void setTaskNumberOfTimesUsed(int uniqueID, int numberOfTimesUsed);


}
