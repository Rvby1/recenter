package com.example.selfcareapplication;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import android.provider.AlarmClock;

public class SleepCalcActivity extends AppCompatActivity {
    private static final String LOG_TAG = SleepCalcActivity.class.getSimpleName();
    private GregorianCalendar calendar;

    private TimePicker sleepCalcTimePicker;
    private TextView sleepTimesTextView;
    private RecyclerView sleepTimesRecyclerView;
    private SleepCalcRecyclerAdapter sleepCalcRecyclerAdapter;

    private final int hoursToAddREM = 1;
    private final int minsToAddREM = 30;
    private final int numSleepTimesToCalculate = 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleep_calc);

        //get views from activity:
        this.sleepCalcTimePicker = findViewById(R.id.sleepTimePicker_sleepCalc);
        this.sleepTimesTextView = findViewById(R.id.sleepTimesTextView_sleepCalc);

        this.sleepTimesRecyclerView = findViewById(R.id.sleepTimesRecyclerView_sleepCalc);
        this.sleepTimesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.sleepCalcRecyclerAdapter = new SleepCalcRecyclerAdapter(this);
        this.sleepTimesRecyclerView.setAdapter(sleepCalcRecyclerAdapter);

        this.calendar = new GregorianCalendar();

        //load sleep times from SharedPreferences?
    }

    /*Sets the calendar to be equal to the data in the clock widget
        Pre-cond:
            calendar is a gregorian calendar defined in the surrounding class
            sleepCalcTimePicker is a timePicker view
        Post-cond:
            calendar's time has been set to the time in the sleepCalcTimePicker
     */
    private void setCalendarToClockTime() {
        int hour, min;

        //TODO: Switch to compat
        if(Build.VERSION.SDK_INT < 23) {
            hour = sleepCalcTimePicker.getCurrentHour();
            min = sleepCalcTimePicker.getCurrentMinute();
        }
        else {
            hour = sleepCalcTimePicker.getHour();
            min = sleepCalcTimePicker.getMinute();
        }

        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, min);

        int AMPM = calendar.get(Calendar.AM_PM);
        if((hour < 12 && AMPM != Calendar.AM) || (hour >= 12 && AMPM != Calendar.PM)) {
            calendar.add(Calendar.HOUR, 12);
        }

        Log.d(LOG_TAG, "Hour: " + hour);
        Log.d(LOG_TAG, "Hour Calendar: " + this.calendar.get(Calendar.HOUR));
        Log.d(LOG_TAG, "Minute: " + min);
        Log.d(LOG_TAG, "AMPM " + this.calendar.get(Calendar.AM_PM));

        Log.d(LOG_TAG, calendar.getTime().toString());
    }

    /*
    Calculates some number of times a user should wake up based on REM cycle based upon the time a user wants to fall asleep.
    *   Pre-cond: 
    *       a user has entered the time they want to fall asleep in the activity's clock
    *   Post-cond: 
    *       a number of wake up times will be printed to the sleepCalc text view
    * */
    public void getWakeUpTimes(View view) {
        setCalendarToClockTime();
        this.findSleepTimes(true, numSleepTimesToCalculate);
        this.sleepTimesTextView.setText(R.string.wakeUpAtTheseTimes_sleepCalc);
    }

    /*
    Will calculate some number of times a user should fall asleep based on REM cycle based upon the time a user wants to wake up.
     *   Pre-cond:
     *       a user has entered the time they want to wake up in the activity's clock
     *   Post-cond:
     *       a number of fall asleep times will be printed to the sleepCalc text view
     * */
    public void getFallAsleepTimes(View view) {
        setCalendarToClockTime();
        this.findSleepTimes(false, numSleepTimesToCalculate);
        this.sleepTimesTextView.setText(R.string.fallAsleepAtTheseTimes_sleepCalc);
    }

    /*
    Finds numCyclesToFind times that a user should wake up/sleep by moving forward/backward the length of a REM cycle from a user-selected time.
        Pre-cond:
            numCyclesToFind is the number of sleep times to find / REM cycles it should move forward/backward.
            findWakeUpTimes is true to find wake up times, but false to find sleep times.
        Post-cond:
            REMCycleTimes will hold the times a user should wake up/sleep. */
    private void findSleepTimes(boolean findWakeUpTimes, int numCyclesToFind) {
        ArrayList<SleepTime> sleepTimesList = new ArrayList<>();
        int addOrSubtractREMCycles = -1;
        if(findWakeUpTimes) {
            addOrSubtractREMCycles = 1;
        }

        for(int i = 0; i < numCyclesToFind; i++) {
            //calendar.roll(Calendar.HOUR, addOrSubtractREMCycles * this.hoursToAddREM);
            //calendar.roll(Calendar.MINUTE, addOrSubtractREMCycles * this.minsToAddREM);
            this.calendar.add(this.calendar.HOUR,  addOrSubtractREMCycles * this.hoursToAddREM);
            this.calendar.add(this.calendar.MINUTE,  addOrSubtractREMCycles * this.minsToAddREM);
            sleepTimesList.add(new SleepTime(this.calendar.get(Calendar.HOUR), this.calendar.get(Calendar.MINUTE), this.calendar.get(Calendar.AM_PM)));
        }

        this.sleepCalcRecyclerAdapter.setSleepTimesList(sleepTimesList);
    }

    public void startAddAlarmActivity(SleepTime timeToCreateAlarmFor) {
        Intent startAddAlarmActivityIntent = new Intent(AlarmClock.ACTION_SET_ALARM);
        startAddAlarmActivityIntent.putExtra(AlarmClock.EXTRA_HOUR, timeToCreateAlarmFor.getHour24HourFormat());
        startAddAlarmActivityIntent.putExtra(AlarmClock.EXTRA_MINUTES, timeToCreateAlarmFor.getMinute());
        if(startAddAlarmActivityIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(startAddAlarmActivityIntent);
        } else {
            Log.d(LOG_TAG, "Could not start alarm activity.");
        }
    }
}
