package com.example.selfcareapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.ImageView;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.images.Artwork;

import java.io.File;
import java.io.IOException;

public class AudioFileWrapper {
    private String fileTitle;
    private String fileArtistName;
    private String fileTrackLength;
    private Artwork fileArtwork;
    private File fileLocation;
    private Bitmap testBitmap;

    private static final String blankAlbumArtUri = "android.resource://com.example.selfcareapplication/drawable/blank_album_art";

    public AudioFileWrapper(File fileLocation) {
        this.fileLocation = fileLocation;
        //get tag information for audio file:
        try {
            //create AudioFile and get tag info:
            AudioFile audioFileToWrap = AudioFileIO.read(this.fileLocation);
            Tag audioFileToWrapTags = audioFileToWrap.getTag();
            AudioHeader audioFileToWrapHeader = audioFileToWrap.getAudioHeader();

            //set file title to title in audio metadata; if nothing, set the file title to the file name.
            this.fileTitle = audioFileToWrapTags.getFirst(FieldKey.TITLE);
            if(this.fileTitle.equals("")) {
                this.fileTitle = this.fileLocation.getName();
            }


            this.fileArtistName = audioFileToWrapTags.getFirst(FieldKey.ARTIST);
            //establish track length in mm:ss format; add 0 before seconds if it is less than 10
            int fileTrackLengthInt = audioFileToWrapHeader.getTrackLength();
            int fileTrackLengthMin = (int) Math.floor(fileTrackLengthInt / 60);
            int fileTrackLengthSec = fileTrackLengthInt % 60;
            this.fileTrackLength = fileTrackLengthMin + ":";
            if(fileTrackLengthSec < 10) {
                this.fileTrackLength += "0" + fileTrackLengthSec;
            } else {
                this.fileTrackLength += fileTrackLengthSec;
            }

            //get file artwork
            this.fileArtwork = audioFileToWrapTags.getFirstArtwork();
            if(this.fileArtwork != null) {
                byte[] artworkByteArray = this.fileArtwork.getBinaryData();
                this.testBitmap = BitmapFactory.decodeByteArray(artworkByteArray, 0, artworkByteArray.length);
            }
        } catch (CannotReadException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TagException e) {
            e.printStackTrace();
        } catch (ReadOnlyFileException e) {
            e.printStackTrace();
        } catch (InvalidAudioFrameException e) {
            e.printStackTrace();
        }
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public String getFileArtistName() {
        return fileArtistName;
    }

    public String getFileTrackLength() {
        return fileTrackLength;
    }

    public File getFileLocation() {
        return fileLocation;
    }

    public void setAlbumArtImageView(ImageView albumArtImageView) {
        if(fileArtwork != null) {
            albumArtImageView.setImageBitmap(testBitmap);
        } else {
            albumArtImageView.setImageURI(Uri.parse(blankAlbumArtUri));
        }
    }
}
